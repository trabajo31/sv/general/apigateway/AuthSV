<?php

/**
 * Laravel - A PHP Framework For Web Artisans
 *
 * @package  Laravel
 * @author   Taylor Otwell <taylor@laravel.com>
 */

 /*
 * ¡IMPORTANTE!
 * INFORMACION AL EQUIPO DE SEGURIDAD: 
 * Esta variable no puede ser movida a una clase, 
 * esto afectaria el funcionamiento del framework, esta configuraion viene con laravel 
*/
$uri = urldecode(

    /** 
     * ¡IMPORTANTE!
     * INFORMACION AL EQUIPO DE SEGURIDAD:
     * Esta variablese accede de esta forma por temas de configuracio del framework laravel, 
     * este archivo no es tocado ni modificado
     * */
    parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)
);

// This file allows us to emulate Apache's "mod_rewrite" functionality from the
// built-in PHP web server. This provides a convenient way to test a Laravel
// application without having installed a "real" web server software here.
if ($uri !== '/' && file_exists(__DIR__.'/public'.$uri)) {
    return false;
}

require_once __DIR__.'/public/index.php';
