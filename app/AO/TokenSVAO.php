<?php

namespace App\AO;

use App\Models\SV\TokenSV;

class TokenSVAO
{

    /**
     * Insertar los datos de sesion en BD
     *
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @description
     * Metodo para ejecutar la consulta en la base de datos
     * Validar usuario y contraseñas
     * @param array user, password
     * @return Model app\Models\SV\VirtualService.php
     */
    public static function setToken($infoToken){
        TokenSV::insert($infoToken);
    }

    /**
     * Verificar si existe la sesión con el token e ip
     *
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @description
     * Metodo para ejecutar la consulta en la base de datos
     * Validar usuario y contraseñas
     * @param array user, password
     * @return Model app\Models\SV\VirtualService.php
     */
    public static function countLoggedRegister($infoToken){
            return TokenSV::where('user_ip', $infoToken['user_ip'])
            ->where('token', $infoToken['token'])
            ->count();
    }

    /**
     * Finalizar una sesión con el token
     *
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @description
     * Metodo para ejecutar la consulta en la base de datos
     * Validar usuario y contraseñas
     * @param array user, password
     * @return Model app\Models\SV\VirtualService.php
     */
    public static function disableToken($token){
        $objTokenn['delected'] = 1;
        TokenSV::where('token', $token)
        ->update($objTokenn);
    }

    


}