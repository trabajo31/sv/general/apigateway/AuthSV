<?php

namespace App\AO;

use App\Models\SV\VirtualService;
use App\Models\Users\InteractionData;

class VirtualServiceAO
{

    /**
     * Obtener los datos de un servicio por su usuario
     *
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @description
     * Metodo para ejecutar la consulta en la base de datos
     * Validar usuario y contraseñas
     * @param array user, password
     * @return Model app\Models\SV\VirtualService.php
     */
    public static function getServiceByName($data) {
        return VirtualService::where('service_user', $data)->get()->first();
    }

    /**
     * Validar usuario y contraseñas en la base de datos
     *
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @description
     * Metodo para ejecutar la consulta en la base de datos
     * Validar usuario y contraseñas
     * @param array user, password
     * @return Model app\Models\SV\VirtualService.php
     */
    public static function validateService($credentials) {
        return VirtualService::where('service_user', $credentials['service_user'])->first();
    }

    /**
     * Validar usuario y contraseñas en la base de datos
     *
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @description
     * Metodo para ejecutar la consulta en la base de datos
     * Validar usuario y contraseñas
     * @param array user, password
     * @return Model app\Models\Users\UserData.php
     */
    public static function saveUserData($objDataUser) {
        return InteractionData::insert($objDataUser);
    }

    /**
     * Validar usuario y contraseñas en la base de datos
     *
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @description
     * Metodo para ejecutar la consulta en la base de datos
     * Validar usuario y contraseñas
     * @param array user, password
     * @return Model app\Models\Users\UserData.php
     */
    public static function getUserData($objDataUser) {
        return InteractionData::where('interacion_id', $objDataUser)->first();
    }

}
