<?php

namespace App\Models\SV;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class TokenSV extends Authenticatable implements JWTSubject
{
    protected $table = 'token_sv';
    
    protected $fillable = [
        'id',
        'user_ip',
        'service_id',
        'token',
        'locked',
        'date_init',
        'delected'
    ];

    public function getJWTIdentifier() {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims() {
        return [];
    }  
    
}
