<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class InteractionData extends Model
{
    protected $table = 'interaction_data';
   
    protected $fillable = [
        'id',
        'interacion_id',
        'user_data',
        'created_at',
    ];
}
