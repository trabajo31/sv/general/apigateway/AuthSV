<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class UserData extends Model
{

    protected $table = 'data_user_table';
   
    protected $fillable = [
        'sesion_id',
        'userData',
    ];

}
