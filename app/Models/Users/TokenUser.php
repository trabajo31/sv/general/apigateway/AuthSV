<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class TokenUser extends Model
{
    protected $table = 'token_users';
   
    protected $fillable = [
        'user_ip',
        'token',
        'user_id',
        'date',
        'delected'
    ];
    
}
