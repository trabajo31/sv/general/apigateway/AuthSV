<?php

namespace App\Helpers\SV;

use App\BL\TokenServiceBL;

class JwtAuthHelper
{
    /**
     * Metodo para el llamado del BL setToken
     * Guardar token en BD
     *
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @description En esta funcion se hace el login del servicio virtual
     * Pasando como parametro service_user y el password
     * @param array $requestHttp con el service_user y el password
     * @return Data $token
     */
    public function saveToken($infoToken){
        TokenServiceBL::setToken($infoToken);
    }
    
    /**
     * Metodo checkear si el token existe en BD
     *
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @description En esta funcion se hace el login del servicio virtual
     * Pasando como parametro service_user y el password
     * @param array $requestHttp con el service_user y el password
     * @return Data $token
     */
    public function checkToken($infoToken){
        return TokenServiceBL::validateConversation($infoToken);
    }
    
}