<?php

namespace App\BL;

use App\AO\TokenSVAO;
use Illuminate\Support\Facades\Log;
use App\Helpers\CurrentDateTime\CurrentDateTime;

class TokenServiceBL {

    private static $response = [];
    private static $excepcion = ['msm' => 'Error al consultar en la Base de Datos', 'status' => 500];

    /**
     * Metodo para armar la logica del objeto
     * a ser almacenado en BD para la sesión
     *
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @description En esta funcion se hace el login del servicio virtual
     * Pasando como parametro service_user y el password
     * @param array $requestHttp con el service_user y el password
     * @return Data $token
     */
    public static function setToken($infoToken) {
        try {
            $timeNow = CurrentDateTime::getCurrentDate();
            $objToken = [
                'user_ip'      => $infoToken['user_ip'],
                'service_id' => $infoToken['id_service'],
                'token'   => $infoToken['token'],
                'locked'  => 0,
                'date_init' => $timeNow,
                'updated_at' => $timeNow,
                'delected' => 0
            ];
            TokenSVAO::setToken($objToken);
        } catch (\Throwable $e) {
            self::$response = self::$excepcion;
            Log::error($e->getMessage() . ' functionon setToken()');
        }
    }

    /**
     * Metodo para validar el token del servicio actual
     *
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @description En esta funcion se hace el login del servicio virtual
     * Pasando como parametro service_user y el password
     * @param array $requestHttp con el service_user y el password
     * @return Data $token
     */
    public static function validateConversation($infoToken) {
        try {
            self::$response = TokenSVAO::countLoggedRegister($infoToken);
        } catch (\Throwable $e) {
            self::$response = self::$excepcion;
            Log::error($e->getMessage() . ' function validateConversation()');
        }
        return self::$response;
    }

}
