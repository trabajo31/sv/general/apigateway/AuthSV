<?php

namespace App\BL;

use App\AO\SV\VirtualServiceAO as SVVirtualServiceAO;
use App\AO\VirtualServiceAO;
use App\Http\Controllers\Generic\ResponseController;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Helpers\SV\JwtAuthHelper;
use App\Helpers\EncryptDecryptHelper;
use Illuminate\Support\Facades\Crypt;

class VirtualServiceBL {

    private static $response = [];
    private static $excepcion = [
        'status' => 500,
        'message' => 'No se pudo comunicar con el servidor, intentelo mas tarde.',
    ];

    /**
     * Metodo para el login de un usuario
     * Method to login a virtual service
     *
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @description En esta funcion se hace el login del servicio virtual
     * Pasando como parametro service_user y el password
     * @param array request Http con el service_user y el password
     * @return Data token
     */
    public static function authLogin($request) {
        
        try {

            $ipClient = request()->ip();

            $credentials = [
                'service_user' => EncryptDecryptHelper::decrypt($request->service_user),
                'password' => EncryptDecryptHelper::decrypt($request->password)
            ];

            if($credentials['service_user'] == null || $credentials['password'] == null){
                $message = 'Credenciales incorrectas.';
                $status = 412;
            } else {
                $message = 'Credenciales correctas.';
                $validateUser = self::validateService($credentials);

                if (!empty($validateUser)) {
    
                    if ($validateUser->delected == 0) {
    
                            $token = Auth::login($validateUser);
                            $objJwtAuthHelper = new JwtAuthHelper();
    
                            $infoToken = [
                                'user'    => $validateUser->id,
                                'user_ip' => $ipClient,
                                'id_service' => $validateUser->id,
                                'token'   => $token
                            ];
    
                            $objJwtAuthHelper->saveToken($infoToken);
                            
                            $data = [
                                'msn' => 'Usuario validado',
                                'token' => $token,
                            ];
    
                            $status = 200;
                            self::$response['data'] = $data;
                    } else {
                        $message = 'El servicio no está activo';
                        $status = 412;
                    }
                } else {
                    $message = "Servicio no reistrado en el sistema";
                    $status = 412;
                }   
            }

            if (isset($message)) {
                self::$response['message'] = $message;
            }
            
            if (isset($status)) {
                self::$response['status'] = $status;
            }

        } catch (\Throwable $th) {
            Log::error(
                $th->getMessage() .
                ' function authLogin() in app\BL\SV\VirtualService\VirtualServiceBL.php'
            );
            self::$response = self::$excepcion;
        }
        return self::$response;
    }

    /**
     * Metodo para obtener la información de un servicio virtual por el nombre de usuario
     *
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @description
     * Metodo para ejecutar la consulta en la base de datos
     * Validar usuario y contraseñas
     * @param usuario user
     * @return Model app\Models\SV\VirtualService.php
     */
    public static function getServiceByName($data) {
        try {
            $objData = VirtualServiceAO::getServiceByName($data);
            self::$response = ['data' => $objData, 'status' => 200];
            Log::info("Successful consultation -> function getServiceByName()");
        } catch (\Throwable $th) {
            self::$response = self::$excepcion;
            Log::error($th->getMessage()." function getServiceByName()");
        }
        return ResponseController::objectResponse(self::$response);
    }

    /**
     * Metodo para Validar de que el sevicio exista
     * Validar usuario y contraseñas
     *
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @description En esta funcion se hace el login del servicio virtual
     * Pasando como parametro service_user y el password
     * @param array user, password
     * @return Model app\Models\SV\VirtualService.php
     */
    public static function validateService($credentials) {
        try {

            $objData = VirtualServiceAO::validateService($credentials);
 
            if(isset($objData)){
                if(Crypt::decryptString($objData->password) == $credentials['password']) {
                    self::$response = ['data' => $objData, 'status' => 200];
                } else {
                    $objData = null;
                }
            }
        } catch (\Throwable $th) {
            self::$response = ['data' => self::$excepcion, 'status' => 500];
            Log::error(
                 $th->getMessage() .
                ' function validateService() in app\BL\SV\VirtualService\VirtualServiceBL.php '
            );
        }
        return $objData;
    }

    public static function saveUserData($objUser) {
        try {
            $saved = VirtualServiceAO::saveUserData($objUser);
            if($saved) {
                $message = "User saved successfully";
                $status = 200;
            } else {
                $message = 'No se pudo almacenar la info del usuario';
                $status = 403;
                $saved = null;
            }
        } catch (\Throwable $th) {
            $status = 500;
            $message = self::$excepcion['message'];
            $saved = null;
            Log::error(
                 $th->getMessage() .
                ' function saveUserData() in app\BL\VirtualServiceBL.php '
            );
        }
        return [
            'status' => $status,
            'message' => $message,
            'data' => $saved
        ];
    }

    public static function findDataUserByID($id)
    {
        try {
            //code... Buscar los datos del usuario por el id
            $data = VirtualServiceAO::getUserData($id);
            $message = 'Exito';
            if($data) {
                $status = 200; 
                $data->user_data = json_decode($data->user_data);
            } else {
                $status = 404;
                $message = 'No se pudo encontrar';
            }
        } catch (\Throwable $th) {
            $status = 500;
            $data = null;
            $message = self::$excepcion['message'];
            Log::error(
                $th->getMessage() .
               ' function findDataUserByID() in app\BL\VirtualServiceBL.php '
           );
        }
        return [
            'status' => $status,
            'data' => $data,
            'msm' => $message
        ];
    }

}
