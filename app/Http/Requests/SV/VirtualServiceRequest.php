<?php

namespace App\Http\Requests\SV;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class VirtualServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstName' => 'max:50|regex:/^[\pL\s]*$/u',
            'subject' => 'max:50',
            'userData.GCTI_LanguageCode' => 'max:50',
            'userData.IpOrigen' => 'max:50',
            'userData.segmento' => 'max:50',
            'userData.tipoId' => 'max:50',
            'userData.Identify' => 'max:50',
            'userData.EmailAddress' => 'max:50',
            'userData.PhoneNumber' => 'max:50',
            'userData.sede' => 'max:50',
            'userData.City' => 'max:50',
            'userData.estado' => 'max:50',
            'userData.motivopsa' => 'max:50',
            'userData.motivoc' => 'max:50',
            'userData.IpOrigen' => 'max:50',
            'userData.terminos' => 'max:50',
            'userData.Send_Chat_Transcript' => 'max:50',
            'userData.FiltroCorreo' => 'max:50',
            'userData.sitio' => 'max:50',
            'userData._genesys_source' => 'max:50',
            'userData._genesys_referrer' => 'max:1024',
            'userData._genesys_url' => 'max:255',
            'userData._genesys_pageTitle' => 'max:50',
            'userData._genesys_browser' => 'max:50',
            'userData._genesys_OS' => 'max:50',
            'userData._genesys_widgets' => 'max:50',
        ];
    }

    public function messages()
    {
        return [
            // 'user.user_name.required' => 'El Usuario de red es obligatoria.',
            // 'user.user_name.max'      => 'El campo Usuario de red  es de maximo 45 caracteres',
            // 'user.user_name.regex'      => 'El campo Usuario de red debe ser texto',
            // 'user.name.required' => 'El Nombre es obligatoria.',
            // 'user.name.max'      => 'El campo Nombre de red es de maximo 100 caracteres',
            // 'user.name.regex'      => 'El campo Nombre de red debe ser solo letras',
            // 'user.area_id.required' => 'La Gerencia es obligatoria.',
            // 'user.email.email'      => 'El Email debe ser valido.',
            // 'user.email.max'      => 'El campo Email es de maximo 100 caracteres',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        
        throw new HttpResponseException(response()->json([
            'status' => 422,
            'errors' => $validator->errors()->all()
        ], 200));
    }
}
