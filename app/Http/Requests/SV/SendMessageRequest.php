<?php

namespace App\Http\Requests\SV;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;

class SendMessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'message' => 'required|max:255|regex:/^[\pL0-9 !@#$%^&*()+{}\[\]\'\'\"\"<>😞😃👍]*$/u',
        ];
    }

    public function messages()
    {
        return [
            'message.required' => 'Campo requerido',
            'message.max'      => 'El campo mensaje solo permite 255 caracteres.',
            'user.user_name.regex'      => 'Mensaje no valido',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'status' => 422,
            'errors' => $validator->errors()->all()
        ], 200));
    }
}
