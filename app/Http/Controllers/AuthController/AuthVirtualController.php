<?php

namespace App\Http\Controllers\AuthController;

use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Requests\SV\LoginVirtualServiceRequest;
use App\BL\VirtualServiceBL;

/**
 * Controlador para la utenticación de un servicio virtual
 *
 *
 * @package App\Http\Controllers\AuthController\SV
 * @author Luis Manuel Hernandez Jimenez (luis.hernandez@grupokonecta.com)
 * @date 01/01/2016
 * */
class AuthVirtualController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register', ['addRol']]]);
    }

    /**
     * Metodo para el login de un servicio virtual
     * Method to login a virtual service
     *
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @description En esta funcion se hace el login del servicio virtual
     * Pasando como parametro service_user y el password
     * @param array $requestHttp con el service_user y el password
     * @return Data $token
     */
    public function login(LoginVirtualServiceRequest $request)
    {
        return VirtualServiceBL::authLogin($request);
    }

    /**
     * Metodo que devuelve el token del usuario actual
     * Method to login a virtual service
     *
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @return JWT $
     */
    public function me()
    {
        return JWTAuth::parseToken()->authenticate();
    }

    /**
     * Metodo que devuelve el token del usuario actual
     * Method to login a virtual service
     *
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @return response $
     */
    public function logout()
    {
        auth()->logout();
        return response()->json(['message' => 'Successfully logged out']);
    }

}