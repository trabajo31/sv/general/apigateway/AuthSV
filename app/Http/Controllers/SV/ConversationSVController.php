<?php

namespace App\Http\Controllers\SV;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Helpers\SV\CurrentVirtualService;
use App\AO\TokenSVAO;
use App\BL\VirtualServiceBL;
use App\Helpers\CurrentDateTime\CurrentDateTime;
use App\Http\Controllers\Generic\ResponseController;
use App\Http\Requests\SV\SendMessageRequest;
use App\Http\Requests\SV\VirtualServiceRequest;
use Illuminate\Support\Facades\Log;
use PhpParser\Node\Stmt\TryCatch;

class ConversationSVController extends Controller
{

    private static $messageException = [
            'status' => 500,
            'message' => 'No se pudo comunicar con el servidor, intentelo mas tarde.',
    ];
    
    public static function credentials()
    {
        return [
            'user' => env('USER_GENESYS', 'usuarioPrueba'),
            'password' => env('PASSWORD_GENESYS', 'prueba*45')
        ];
    }

    /**
     * Funcion para inicializar el chat
     *
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param user $
     * @param password $
     * @return Boolean $
     */
    public function startChat(VirtualServiceRequest $request)
    {
        try {
            $credentials = self::credentials();

            $urlGenesys =  env('APP_ENDPOINT_GENESYS') . 
            CurrentVirtualService::getCurrentService()->service_strategy;

            $response = Http::withBasicAuth($credentials['user'], $credentials['password'])
            	->withOptions(['verify' => false])
		        ->asForm()
                ->post($urlGenesys, $request->all());

        } catch (\Throwable $th) {
		dd($th);
            Log::error(
                $th->getMessage() .
                ' function startChat() in app\Http\Controllers\SV\ConversationSVController.php'
            );
            return response()->json(self::$messageException);
        }
        return $response->json();
    }

    /**
     * Funcion para optener el limite de los archivos
     *
     *
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param user $
     * @param password $
     * @return Boolean $
     */
    public function fileLimit(Request $request, $id) {
        
        try {
            $credentials = self::credentials();
            
            $urlGenesys =  env('APP_ENDPOINT_GENESYS') .
                CurrentVirtualService::getCurrentService()
                ->service_strategy . '/' . $id . '/file/limits';

            $response = Http::withBasicAuth($credentials['user'], $credentials['password'])
            	->withOptions(['verify' => false])
		        ->asForm()
                ->post($urlGenesys, $request->all());
            
        } catch (\Throwable $th) {
            Log::error(
                $th->getMessage() .
                ' function fileLimit() in app\Http\Controllers\SV\ConversationSVController.php'
            );
            return response()->json(self::$messageException);
        }
        return $response->json();
    }

    /**
     * Funcion para refrescar la conversacion, optener los mensajes
     *
     *
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param user $
     * @param password $
     * @return Boolean $
     */
    public function refreshChat(Request $request, $id)
    {
        try {
            $credentials = self::credentials();

            $urlGenesys =  env('APP_ENDPOINT_GENESYS') .
                CurrentVirtualService::getCurrentService()
                ->service_strategy . '/' . $id . '/refresh';

            $response = Http::withBasicAuth($credentials['user'], $credentials['password'])
            	->withOptions(['verify' => false])
		        ->asForm()
                ->post($urlGenesys, $request->all());
        } catch (\Throwable $th) {
            Log::error(
                $th->getMessage() .
                ' function refreshChat() in app\Http\Controllers\SV\ConversationSVController.php');
            return response()->json(self::$messageException);
        }
        return $response->json();
    }

    /**
     * Funcion para iniciar el tipeo del usuario
     *
     *
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param user $
     * @param password $
     * @return Boolean $
     */
    public function startTyping(Request $request, $id)
    {
        try {
            $credentials = self::credentials();

            $urlGenesys =  env('APP_ENDPOINT_GENESYS') .
                CurrentVirtualService::getCurrentService()
                ->service_strategy . '/' . $id . '/startTyping';

            $response = Http::withBasicAuth($credentials['user'], $credentials['password'])
            	->withOptions(['verify' => false])
		        ->asForm()
                ->post($urlGenesys, $request->all());

        } catch (\Throwable $th) {
            Log::error(
                $th->getMessage() .
                ' function startTyping() in app\Http\Controllers\SV\ConversationSVController.php'
            );
            return response()->json(self::$messageException);
        }
        return $response->json();
    }

    /**
     * Funcion para detener el tipeo del usuario
     *
     *
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param user $
     * @param password $
     * @return Boolean $
     */
    public function stopTyping(Request $request, $id)
    {
        try {
            $credentials = self::credentials();

            $urlGenesys = env('APP_ENDPOINT_GENESYS') . 
                CurrentVirtualService::getCurrentService()
                ->service_strategy . '/' . $id . '/stopTyping';

            $response = Http::withBasicAuth($credentials['user'], $credentials['password'])
            	->withOptions(['verify' => false])
		        ->asForm()
                ->post($urlGenesys, $request->all());

        } catch (\Throwable $th) {
            Log::error(
                $th->getMessage() .
                ' function stopTyping() in app\Http\Controllers\SV\ConversationSVController.php'
            );
            return response()->json(self::$messageException);
        }
        return $response->json();
    }

    /**
     * Funcion para enviar un mensaje
     *
     *
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param user $
     * @param password $
     * @return Boolean $
     */
    public function sendMessage(SendMessageRequest $request, $id)
    {
        try {
            $credentials = self::credentials();

            $urlGenesys =  env('APP_ENDPOINT_GENESYS') .
                CurrentVirtualService::getCurrentService()
                ->service_strategy . '/' . $id . '/send';

            $response = Http::withBasicAuth($credentials['user'], $credentials['password'])
            	->withOptions(['verify' => false])
		        ->asForm()
                ->post($urlGenesys, $request->all());
            
        } catch (\Throwable $th) {
            Log::error(
                $th->getMessage() .
                ' function sendMessage() in app\Http\Controllers\SV\ConversationSVController.php'
            );
            return response()->json(self::$messageException);
        }
        return $response->json();
    }

    /**
     * Funcion para enviar un archivo
     *
     *
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param user $
     * @param password $
     * @return Boolean $
     */
    public function sendFile(Request $request, $id)
    {
        try {
            $credentials = self::credentials();

            $urlGenesys = env('APP_ENDPOINT_GENESYS') . 
                CurrentVirtualService::getCurrentService()
                ->service_strategy . '/' . $id . '/file';

            $response = Http::withBasicAuth($credentials['user'], $credentials['password'])
            	->withOptions(['verify' => false])
		        ->asForm()
                ->post($urlGenesys, $request->all());
        
        } catch (\Throwable $th) {
            Log::error(
                $th->getMessage() .
                ' function sendFile() in app\Http\Controllers\SV\ConversationSVController.php'
            );
            return response()->json(self::$messageException);
        }
        return $response;
    }

    /**
     * Funcion para finalizar la conversación
     *
     *
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param user $
     * @param password $
     * @return Boolean $
     */
    public function disconnectConversation(Request $request, $id)
    {
        try {
            $credentials = self::credentials();

            $urlGenesys = env('APP_ENDPOINT_GENESYS') . 
                CurrentVirtualService::getCurrentService()
                ->service_strategy . '/' . $id . '/disconnect';

            $response = Http::withBasicAuth($credentials['user'], $credentials['password'])
            	->withOptions(['verify' => false])
		        ->asForm()
                ->post($urlGenesys, $request->all());
            TokenSVAO::disableToken($request->bearerToken());

        } catch (\Throwable $th) {
            Log::error(
                $th->getMessage() .
                ' function disconnectConversation() in ' +
                'app\Http\Controllers\SV\ConversationSVController.php'
            );
            return response()->json(self::$messageException);
        }
        return $response->json();
    }

    /**
     * FunciÃ³n para armar el objeto a ser almacenado en BD
     *
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param user $
     * @param password $
     * @return Boolean $
     */
    public function saveUser(Request $request)
    {
        try {
            $objectToSave = [
                'interacion_id' => $request->SesionID,
                'user_data' => json_encode($request->UserData),
                'created_at' => CurrentDateTime::getCurrentDate()
            ];
            $saved = VirtualServiceBL::saveUserData($objectToSave);
        } catch (\Throwable $th) {
            Log::error(
                $th->getMessage() .
                ' function saveUser() in ' +
                'app\Http\Controllers\SV\ConversationSVController.php'
            );
            return response()->json(self::$messageException);
        }

        return ResponseController::objectResponse($saved);
    }

    /**
     * Función para armar el objeto a ser almacenado en BD
     *
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param user $
     * @param password $
     * @return Boolean $
     */
    public function findDataUserByID(Request $request)
    {
        try {
            $SesionID = $request->SesionID;
            $objUser = VirtualServiceBL::findDataUserByID($SesionID);
        } catch (\Throwable $th) {
            Log::error(
                $th->getMessage() .
                ' function saveUser() in ' +
                'app\Http\Controllers\SV\ConversationSVController.php'
            );
            return response()->json(self::$messageException);
        }
        return ResponseController::objectResponse($objUser);
    }
}
