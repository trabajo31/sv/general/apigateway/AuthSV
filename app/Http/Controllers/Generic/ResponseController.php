<?php

namespace App\Http\Controllers\Generic;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ResponseController
{

    /**
     * Metodo para la respuesta cuando se hagan demasiadas peticiones
     *
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @description En esta funcion se hace el login del servicio virtual
     * Pasando como parametro service_user y el password
     * @param array $requestHttp con el service_user y el password
     * @return Data $token
     */
    public static function howManyRequests(Request $request) {
        Log::info("Many Requests -> function howManyRequests() in RouteServiceProvider - IP: " . $request->ip());
        $response = [];
        $response['status'] = 412;
        $response['msm'] = 'Usted ha realizado demasiadas peticiones';
        return response()->json($response, 200);
    }

    /**
     * Metodo para la respuesta general
     *
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @description En esta funcion se hace el login del servicio virtual
     * Pasando como parametro service_user y el password
     * @param array $requestHttp con el service_user y el password
     * @return Data $token
     */
    public static function objectResponse($res) {
        $response = [];
        if ($res['status'] === 500 || $res['status'] === 404 || $res['status'] === 403) {
            $data   = null;
            $msm    = $res['msm'];
            $status = $res['status'];
        } else {
            $data   = $res['data'];
            $msm    = 'Éxito';
            $status = 200;
        }
        $response['data'] = $data;
        $response['message'] = $msm;
        $response['status'] = $status;

        return response()->json($response);
    }
}
