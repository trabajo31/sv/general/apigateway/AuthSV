<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController\AuthVirtualController;
use App\Http\Controllers\SV\ConversationSVController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


    /**
     * Ruta para la autenticacion de un servicio virtual
     * Routes to authenticate a services virtual
     *
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param user $
     * @param password $
     * @return  $
     */
    Route::post('auth/loginService', [AuthVirtualController::class, 'login']);

    /**
     * Rutas que pasan por el middleware JwtAuth
     *
     * @date 29/12/2021
     * @author Luis Manuel Hernandez Jimenez
     */
    Route::group(['middleware' => 'JwtAuth'], function () {

        /**
         * Rutas que pasan por el middleware throttle:IpRequests
         * Ubicado en app\Providers\RouteServiceProvider.php
         *
         * @date 29/12/2021
         * @author Luis Manuel Hernandez Jimenez
         */
        Route::group(['middleware' => 'throttle:IpRequests'], function () {


            Route::post('/host', [ConversationSVController::class, 'StartChat']);

            Route::post('/host/{id}/file/limits', [ConversationSVController::class, 'FileLimit']);

            Route::post('/host/{id}/startTyping', [ConversationSVController::class, 'startTyping']);

            Route::post('/host/{id}/stopTyping', [ConversationSVController::class, 'stopTyping']);

            Route::post('/host/{id}/send', [ConversationSVController::class, 'sendMessage']);

            Route::post('/host/{id}/disconnect', [ConversationSVController::class, 'disconnectConversation']);

            Route::post('/host/{id}/file', [ConversationSVController::class, 'sendFile']);

            Route::post('/host/saveUser', [ConversationSVController::class, 'saveUser']);

            Route::post('/host/getData', [ConversationSVController::class, 'findDataUserByID']);

        });

        Route::post('/host/{id}/refresh', [ConversationSVController::class, 'refreshChat']);

    });
